

# BiSEPS Help #

This repo contains various help files, hints, tips and screencasts

to help run the Binary Population Synthesis Code 'BiSEPS'



# Download BiSEPS #

Go to the repostiory [get_biseps](https://bitbucket.org/biseps/get_biseps)




# BiSEPS Video Tutorials#

[Screencasts](https://www.youtube.com/channel/UCgYu8eWb7f_parhIQXMEhcA/playlists)



# Useful Links #

[Software Carpentry](http://software-carpentry.org)

[Git Videos](http://git-scm.com/videos)

[Python Videos](http://pyvideo.org/)


