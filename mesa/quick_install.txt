
MESA HOME:
    http://mesa.sourceforge.net/index.html

MESASDK:
    http://www.astro.wisc.edu/~townsend/static.php?ref=mesasdk#Download


Change to proper directory:

    cd /padata/beta/users/pmr257/repos



Download SDK for 15-7-2014:

    curl -O http://www.astro.wisc.edu/~townsend/resource/download/mesasdk/mesasdk-x86_64-linux-20140715.tar.gz


Unzip SDK

    tar -xvf mesasdk-x86_64-linux-20140715.tar.gz


Download MESA source code version 6794:

    wget http://sourceforge.net/projects/mesa/files/releases/mesa-r6794.zip


Unzip source code:

    unzip mesa-r6794.zip


Setup Mesa environment variables:

    gedit /home/physastro/pmr257/handy_scripts/env_mesa.sh

    check the two variables are correct:
    $MESA_DIR
    $MESASDK_ROOT

    source env_mesa.sh


Compile/install MESA:

    cd /padata/beta/users/pmr257/repos/mesa-r6794
    ./install


